//
//  SearchModel.m
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import "Search.h"
#import "Location.h"
#import "AFNetworking.h"
#import "AFJSONRequestOperation.h"

@implementation Search {
    Location *hroch;
    Location *zavesenykafe;
    Location *kuzelka;
    Location *piknikampa;
    Location *ucostella;
    Location *cajovnasiva;
    Location *starbucks;
    Location *louvre;
    Location *cestovatele;
    Location *bajkazyl;
    Location *giallorossa;
    Location *grossetto;
}

- (id)init {
    self = [super init];
    if (self) {
        _filterModel = [Location locationTypes];
        _filterState = @{}.mutableCopy;
        for (int i = 0; i < _filterModel.count; i++) {
            _filterState[(NSNumber *)_filterModel[i]] = @(YES);
        }
        _contextEnabled = YES;
    }
    return self;
}

- (void)createDefaultLocations {
    hroch = [[Location alloc] initWithLatitude:50.089186 longitude:14.40358 distance:10 name:@"U Hrocha" teaser:@"Nejlepší Plzeň v Praze a svérázná obsluha" type:LocationPub];
    zavesenykafe = [[Location alloc] initWithLatitude:50.088442 longitude:14.395759 distance:15 name:@"U Zavěšenýho kafe" teaser:@"Výborná Burianova káva" type:LocationCafe];
    kuzelka = [[Location alloc] initWithLatitude:50.087719 longitude:14.407443 distance:25 name:@"Lokál u Bílé kuželky" teaser:@"Skvělé Davelské párky a žebírka" type:LocationRestaurant];
    piknikampa = [[Location alloc] initWithLatitude:50.083837 longitude:14.40755 distance:30 name:@"Piknik na Kampě" teaser:@"V okolí je několik take-away restaurací" type:LocationPiknik];
    ucostella = [[Location alloc] initWithLatitude:50.087589 longitude:14.404428 distance:40 name:@"U Costella" teaser:@"Dejte si lasagne nebo pizzu" type:LocationPizza];
    cajovnasiva = [[Location alloc] initWithLatitude:50.089137 longitude:14.423869 distance:70 name:@"Čajovna Šiva" teaser:@"Užijte si v kamenných sklepech vodní dýmku a čaj" type:LocationTeahouse];
    starbucks = [[Location alloc] initWithLatitude:50.087947 longitude:14.404192 distance:90 name:@"Starbucks" teaser:@"Zahřejte se Americanem" type:LocationCafe];
    louvre = [[Location alloc] initWithLatitude:50.082281 longitude:14.41976 distance:120 name:@"Café Louvre" teaser:@"Dejte si Kávu a zahrajte kulečník" type:LocationCafe];
    cestovatele = [[Location alloc] initWithLatitude:50.079217 longitude:14.414384 distance:130 name:@"Klub Cestovatelů" teaser:@"Dejte si rozsezenou müsli tyčinku a vodu z Vltavy." type:LocationTeahouse];
    bajkazyl = [[Location alloc] initWithLatitude:50.07269 longitude:14.413784 distance:150 name:@"Bajkazyl" teaser:@"Posezení u Vltavy s pivem či limonádou." type:LocationOutdoor];
    giallorossa = [[Location alloc] initWithLatitude:50.088146 longitude:14.425145 distance:200 name:@"Giallo Rossa" teaser:@"Tenká lahodná pizza v pizzerii fandící AS Roma." type:LocationPizza];
    grossetto = [[Location alloc] initWithLatitude:50.085627 longitude:14.425853 distance:300 name:@"Grossetto" teaser:@"Výborné lilkové lasagne." type:LocationPizza];
}

- (void)changeToRandomContext {
    _context = _context+1 % 4;
}

- (NSString *)contextName {
    switch (_context) {
        case RainyContext:
            return @"It's raining a lot";
        case SunnyContext:
            return @"It's sunny";
        case CranberryContext:
            return @"Cranberry season";
        case UnknownSearchContext:
        default:
            return @"It's a nice day";
    }
}

- (void)toggleFilterStateForIndex:(NSInteger)index; {
    NSNumber *type = _filterModel[index];
    BOOL lastState = [_filterState[type] boolValue];
    _filterState[type] = @(!lastState);
    if (index == 0) {
        for (int i = 0; i < _filterState.count; i++) {
            NSNumber *t = _filterModel[i];
            _filterState[t] = @(!lastState);
        }
    } else {
        BOOL areAllSelected = YES;
        for (int i = 1; i < _filterState.count; i++) {
            NSNumber *t = _filterModel[i];
            if (![_filterState[t] boolValue]) {
                areAllSelected = NO;
                break;
            }
        }
        _filterState[@(LocationAll)] = @(areAllSelected);
    }
}

- (NSArray *)nearbyRestaurants {
    NSArray *result = nil;
    if (!_context) {
        result = [self nearbyRestaurantsNormal];
    } else {
        switch (_context) {
            case RainyContext:
                result = [self nearbyRestaurantsForRainyWeather];
                break;
            case SunnyContext:
                result = [self nearbyRestaurantsForSunnyWeather];
                break;
            case CranberryContext:
                result = [self nearbyRestaurantsForBrusinkySeason];
                break;
            case UnknownSearchContext:
            default:
                result = [self nearbyRestaurantsNormal];
                break;
        }
    }
    return [self filterResults:result];
}

- (NSArray *)filterResults:(NSArray *)results {
    NSMutableArray *allowed = [NSMutableArray arrayWithCapacity:results.count];
    for (Location *l in results) {
        BOOL isLocationAllowed = [[self.filterState objectForKey:@(l.type)]boolValue];
        BOOL allAreAllowed = [[self.filterState objectForKey:@(LocationAll)]boolValue];
        if (isLocationAllowed || allAreAllowed) {
            [allowed addObject:l];
        }
    }
    return allowed;
}

- (NSArray *)nearbyRestaurantsNormal {
    [self createDefaultLocations];
    return @[hroch, zavesenykafe, kuzelka, piknikampa, ucostella, cajovnasiva, starbucks, louvre, cestovatele, bajkazyl, giallorossa, grossetto];
}

- (NSArray *)nearbyRestaurantsForRainyWeather {
    [self createDefaultLocations];
    zavesenykafe.teaser = @"Přijďte se schovat před deštěm a zahřát se dobrou kávou";
    return @[zavesenykafe, cajovnasiva, starbucks, hroch, kuzelka, ucostella, louvre,cestovatele,  bajkazyl, piknikampa, giallorossa, grossetto];
}

- (NSArray *)nearbyRestaurantsForSunnyWeather {
    [self createDefaultLocations];
    bajkazyl.teaser = @"Přijďte se schladit pivem k Vltavě";
    return @[bajkazyl, piknikampa, zavesenykafe, cajovnasiva, starbucks, hroch, kuzelka, ucostella, louvre,cestovatele, giallorossa, grossetto];
}

- (NSArray *)nearbyRestaurantsForBrusinkySeason {
    [self createDefaultLocations];
    louvre.distance = 4;
    louvre.teaser = @"Zveme vás na brusinkové Crème brûlée";
    return @[louvre, zavesenykafe, cajovnasiva, starbucks, hroch, kuzelka, ucostella, cestovatele,  bajkazyl, piknikampa, giallorossa, grossetto];
}

- (void)requestForContext:(NSString *)contextId {
    NSURL *url = [NSURL URLWithString:@"https://ondra.macoszek.cz/search"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"App.net Global Stream: %@", JSON);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSearchModelUpdatedNotification object:nil];
    } failure:nil];
    [operation start];
}

@end
