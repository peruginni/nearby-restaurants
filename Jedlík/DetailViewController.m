//
//  DetailViewController.m
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/11/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *sectionSegmentedControl;

- (IBAction)sectionChanged:(id)sender;
- (IBAction)swipeLeft:(id)sender;
- (IBAction)swipeRight:(id)sender;

@end

@implementation DetailViewController {
    UIViewController *_contactController;
    UIViewController *_menuController;
    UIViewController *_photosController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _contactController = [[self storyboard] instantiateViewControllerWithIdentifier:@"DetailContact"];
    [self addChildViewController:_contactController];
    _menuController = [[self storyboard] instantiateViewControllerWithIdentifier:@"DetailMenu"];
    [self addChildViewController:_menuController];
    _photosController = [[self storyboard] instantiateViewControllerWithIdentifier:@"DetailPhotos"];
    [self addChildViewController:_photosController];
}

- (void)viewDidAppear:(BOOL)animated {
    _contactController.view.frame = CGRectMake(0, 0, 320, _containerView.frame.size.height);
    _menuController.view.frame = CGRectMake(0, 0, 320, _containerView.frame.size.height);
    _photosController.view.frame = CGRectMake(0, 0, 320, _containerView.frame.size.height);
    _sectionSegmentedControl.selectedSegmentIndex = 1;
    [self sectionChanged:nil];
}

- (IBAction)sectionChanged:(id)sender {
    [_contactController.view removeFromSuperview];
    [_menuController.view removeFromSuperview];
    [_photosController.view removeFromSuperview];
    
    switch (_sectionSegmentedControl.selectedSegmentIndex) {
        case 0:
            [self.containerView addSubview:_photosController.view];
            break;
        case 1:
            [self.containerView addSubview:_menuController.view];
            break;
        case 2:
        default:
            [self.containerView addSubview:_contactController.view];
            break;
    }
}

- (IBAction)swipeLeft:(id)sender {
    NSInteger i = _sectionSegmentedControl.selectedSegmentIndex;
    if (i == 2) {
        return;
    }
    _sectionSegmentedControl.selectedSegmentIndex++;
    [self sectionChanged:nil];
}

- (IBAction)swipeRight:(id)sender {
    NSInteger i = _sectionSegmentedControl.selectedSegmentIndex;
    if (i == 0) {
        return;
    }
    _sectionSegmentedControl.selectedSegmentIndex--;
    [self sectionChanged:nil];
}

@end
