//
//  LocationCell.m
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import "LocationCell.h"

@interface LocationCell ()

@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *shortDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;

@end

@implementation LocationCell

- (void)setupForLocation:(Location *)location {
    _nameLabel.text = location.name;
    _shortDescriptionLabel.text = location.teaser;
    _distanceLabel.text = [NSString stringWithFormat:@"%d m", (int)location.distance];
    switch (location.type) {
        case LocationCafe:
            _iconImageView.image = [UIImage imageNamed:@"cafe.png"];
            break;
        case LocationTeahouse:
            _iconImageView.image = [UIImage imageNamed:@"teahouse.png"];
            break;
        case LocationOutdoor:
        case LocationPiknik:
            _iconImageView.image = [UIImage imageNamed:@"outdoor.png"];
            break;
        case LocationPub:
            _iconImageView.image = [UIImage imageNamed:@"pub.png"];
            break;
        case LocationPizza:
            _iconImageView.image = [UIImage imageNamed:@"pizza.png"];
            break;
        case LocationRestaurant:
        case LocationAll:
        case LocationOther:
        default:
            _iconImageView.image = [UIImage imageNamed:@"restaurant.png"];
            break;
    }
}

@end
