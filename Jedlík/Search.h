//
//  SearchModel.h
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SearchContext) {
    UnknownSearchContext,
    RainyContext,
    SunnyContext,
    CranberryContext
};

#define kSearchModelUpdatedNotification @"kSearchModelUpdatedNotification"

@interface Search : NSObject

@property (strong, nonatomic) NSArray *filterModel;
@property (strong, nonatomic) NSMutableDictionary *filterState;
@property (assign, nonatomic) SearchContext context;
@property (assign, nonatomic) BOOL contextEnabled;

- (NSArray *)nearbyRestaurants;

- (void)changeToRandomContext;
- (NSString *)contextName;
- (void)toggleFilterStateForIndex:(NSInteger)index;

@end
