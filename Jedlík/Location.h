//
//  Restaurant.h
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LocationType) {
    LocationOther,
    LocationRestaurant,
    LocationCafe,
    LocationBar,
    LocationPiknik,
    LocationFastFood,
    LocationTeahouse,
    LocationPub,
    LocationVeg,
    LocationPizza,
    LocationAll,
    LocationOutdoor
};

@interface Location : NSObject

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double distance;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *teaser;
@property (nonatomic, assign) LocationType type;

- (id)initWithLatitude:(double)lat longitude:(double)lon distance:(double)dist name:(NSString *)name teaser:(NSString *)teaser type:(enum LocationType)type;
+ (NSString *)stringFromLocationType:(LocationType)locationType;
+ (NSArray *)locationTypes;

@end
