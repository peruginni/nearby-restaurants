//
//  LocationCell.h
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"

@interface LocationCell : UITableViewCell

- (void)setupForLocation:(Location *)location;

@end
