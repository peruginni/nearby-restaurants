//
//  main.m
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
