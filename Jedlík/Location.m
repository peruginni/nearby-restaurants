//
//  Restaurant.m
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import "Location.h"

@implementation Location

- (id)initWithLatitude:(double)lat longitude:(double)lon distance:(double)dist name:(NSString *)name teaser:(NSString *)teaser type:(enum LocationType)type {
    self = [super init];
    if (self) {
        _latitude = lat;
        _longitude = lon;
        _distance = dist;
        _name = name;
        _teaser = teaser;
        _type = type;
    }
    return self;
}

+ (NSString *)stringFromLocationType:(LocationType)locationType {
    switch (locationType) {
        case LocationRestaurant:
            return @"Restaurant";
        case LocationCafe:
            return @"Café";
        case LocationBar:
            return @"Bar";
        case LocationPiknik:
            return @"Picnic";
        case LocationFastFood:
            return @"Fast food";
        case LocationTeahouse:
            return @"Tea house";
        case LocationPub:
            return @"Pubs";
        case LocationVeg:
            return @"Vegetarian";
        case LocationOutdoor:
            return @"Outdoor";
        case LocationAll:
            return @"All";
        case LocationOther:
        default:
            return @"Other";
            break;
    }
}

+ (NSArray *)locationTypes {
    return @[@(LocationAll), @(LocationRestaurant), @(LocationCafe), @(LocationBar),
             @(LocationPiknik), @(LocationFastFood), @(LocationTeahouse),
             @(LocationPub), @(LocationVeg), @(LocationOutdoor), @(LocationOther)];
}

@end
