//
//  MapPointAnnotation.h
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/11/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MapPointAnnotation : MKPointAnnotation

@property (strong, nonatomic) Location *locationEntity;

@end
