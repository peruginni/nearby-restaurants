//
//  SearchViewController
//  Jedlík
//
//  Created by Ondrej Macoszek on 5/6/13.
//  Copyright (c) 2013 Ondřej Macoszek. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "SearchViewController.h"
#import "MapPointAnnotation.h"
#import "LocationCell.h"
#import "MBProgressHUD.h"
#import "PopoverView.h"


@interface SearchViewController () <UITableViewDataSource, UITableViewDelegate, PopoverViewDelegate, MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *resultsTable;
@property (strong, nonatomic) IBOutlet UITableView *filterTable;
@property (strong, nonatomic) IBOutlet MKMapView *map;
@property (strong, nonatomic) IBOutlet UIView *viewWrapper;
@property (strong, nonatomic) IBOutlet UIView *contextWrapper;
@property (strong, nonatomic) IBOutlet UILabel *contextTitle;
@property (strong, nonatomic) IBOutlet UISwitch *contextSwitch;
@property (strong, nonatomic) IBOutlet UISegmentedControl *viewSegmentedControl;

- (IBAction)filterTapped:(id)sender;
- (IBAction)viewSegmentedControlChanged:(id)sender;
- (IBAction)threeTapsPerformed:(id)sender;
- (IBAction)contextSwitchValueChanged:(id)sender;

@end

@implementation SearchViewController {
    Search *_searchModel;
    NSArray *_results;
    NSMutableDictionary *_mapShortcutToEntities;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _searchModel = [[Search alloc] init];
    [self refreshResults];
}

- (void)viewWillAppear:(BOOL)animated {
    [_resultsTable setContentOffset:CGPointMake(0,40)];
    CGRect rect = _resultsTable.frame;
    rect.size.height = self.view.frame.size.height - _contextWrapper.frame.size.height;
    _resultsTable.frame = rect;
    
    rect = _map.frame;
    rect.size.height = self.view.frame.size.height - _contextWrapper.frame.size.height;
    _map.frame = rect;
}

- (IBAction)filterTapped:(id)sender {
    _filterTable.frame = CGRectMake(0, 50, 280, 360);
    [_filterTable removeFromSuperview];
    [PopoverView showPopoverAtPoint:CGPointMake(20, 55) inView:self.view withContentView:_filterTable delegate:self];
}

- (IBAction)viewSegmentedControlChanged:(id)sender {
    if (_viewSegmentedControl.selectedSegmentIndex == 0) {
        [_viewWrapper bringSubviewToFront:_resultsTable];
    } else {
        [_viewWrapper bringSubviewToFront:_map];
        
        MKCoordinateRegion region;
        region.center = _map.userLocation.coordinate;
        
        MKCoordinateSpan span;
        span.latitudeDelta  = 0.01; // Change these values to change the zoom
        span.longitudeDelta = 0.01;
        region.span = span;
        
        [_map setRegion:region animated:YES];
    }
}

- (IBAction)threeTapsPerformed:(id)sender {
    [_searchModel changeToRandomContext];
    [self refreshResults];
}

- (IBAction)contextSwitchValueChanged:(id)sender {
    _searchModel.contextEnabled = _contextSwitch.on;
    [self refreshResults];
}

- (void)refreshResults {
    _results = [_searchModel nearbyRestaurants];
    _contextSwitch.on = _searchModel.contextEnabled;
    _contextTitle.text = [_searchModel contextName];
    [_resultsTable reloadData];
    [_filterTable reloadData];
    
    _mapShortcutToEntities = [NSMutableDictionary dictionary];
    [_map removeAnnotations:_map.annotations];
    for (Location *l in _results) {
        CLLocationCoordinate2D location;
        location.latitude = l.latitude;
        location.longitude = l.longitude;
        MapPointAnnotation *a = [[MapPointAnnotation alloc] init];
        a.coordinate = location;
        a.title = l.name;
        a.locationEntity = l;
        [_map addAnnotation:a];
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _resultsTable) {
        return _results.count;
    } else if (tableView == _filterTable) {
        return _searchModel.filterModel.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _resultsTable) {
        Location *location = [_results objectAtIndex:indexPath.row];
        LocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationCell"];
        [cell setupForLocation:location];
        return cell;
    } else if (tableView == _filterTable) {
        enum LocationType locationType = [[_searchModel.filterModel objectAtIndex:indexPath.row] integerValue];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCell"];
        cell.textLabel.text = [Location stringFromLocationType:locationType];
        if ([[_searchModel.filterState objectForKey:@(locationType)] boolValue]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        return cell;
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.filterTable) {
        [_searchModel toggleFilterStateForIndex:indexPath.row];
        [self refreshResults];
    }
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if (![annotation isKindOfClass:[MapPointAnnotation class]]) {
        return nil;
    }
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"pinView"];
    if (!pinView) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pinView"];
        pinView.pinColor = MKPinAnnotationColorRed;
        pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pinView.rightCalloutAccessoryView = rightButton;
    } else {
        pinView.annotation = annotation;
    }
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    [self performSegueWithIdentifier:@"Detail" sender:nil];
}

@end
