Nearby restaurants
==================

Idea
----
Show nearby **restaurants according to current user context**. If user is in location where **is raining**, show her/him indoor restaurants with hot beverages. If is **sunny weather**, show him riverside pubs. If is cranberry season, show restaurants with specific meals. 

App was developed as part of **school project** to exercise UI development and user testing. To change user context in app please triple tap on bottom bar.

** [Video demonstration](https://www.youtube.com/watch?v=aXyYPU4C_5U) **

Project Structure
-----------------

![xcode structure](http://cl.ly/image/2c1y1H1p0s3s/structure.png)

Video demonstration
-------------------

** [Watch on YouTube](https://www.youtube.com/watch?v=aXyYPU4C_5U) **

Screenshots
-----------

![Map view](http://cl.ly/image/3b101Z1t092u/map.png)

![List view](http://cl.ly/image/3j122m3u3R1H/list.png)

![Contact for restaurant detail](http://cl.ly/image/0k1a3i2l2W2d/contact.png)


